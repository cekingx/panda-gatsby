import React from "react"
import { graphql } from "gatsby"

import Layout from "../components/Layout"

export default ({ data }) => {
  return (
    <Layout>
      <div>
        <h1>My Files</h1>
        <table>
          <thead>
            <tr>
              <th>Relative Path</th>
              <th>Extension</th>
              <th>Size</th>
              <th>Birth Time</th>
            </tr>
          </thead>
          <tbody>
            {data.allFile.edges.map(({ node }, index) => (
              <tr key={index}>
                <td>{node.relativePath}</td>
                <td>{node.extension}</td>
                <td>{node.prettySize}</td>
                <td>{node.birthTime}</td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </Layout>
  )
}

export const query = graphql`
  query {
    allFile {
      edges {
        node {
          relativePath
          extension
          prettySize
          birthTime(fromNow: true)
        }
      }
    }
  }
`
